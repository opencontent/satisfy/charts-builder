from controllers.HasuraClient import HasuraClient
from fastapi import FastAPI, status, Depends
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
from starlette_exporter import PrometheusMiddleware, handle_metrics
from pythonjsonlogger import jsonlogger
from datetime import datetime, timedelta
from api.api_docs import RatingsParams, RatingDetailParams, SingleStatParams
from utils.data_builder import *
from api.api_responses import *

from isoweek import Week
from typing import List, Optional
from dotenv import load_dotenv

import os
import json
import uvicorn
import logging
import locale
import calendar
import dateutil.relativedelta

# instantiate the API
app = FastAPI()

load_dotenv()

FASTAPI_APP_HOST = os.getenv('FASTAPI_APP_HOST', default='0.0.0.0')
FASTAPI_APP_PORT = os.getenv('FASTAPI_APP_PORT', default=5006)
PROMETHEUS_JOB_NAME = os.getenv('PROMETHEUS_JOB_NAME', default='satisfy_charts_builder')
HASURA_ENDPOINT = os.getenv('HASURA_ENDPOINT', default='https://satisfy.hasura.app/v1/graphql')
HASURA_SECRET = os.getenv('HASURA_SECRET')
MAX_DAYS = os.getenv('MAX_DAYS', default=180)
MAX_WEEKS = os.getenv('MAX_WEEKS', default=52)
MAX_MONTHS = os.getenv('MAX_MONTHS', default=18)
MAX_YEARS = os.getenv('MAX_YEARS', default=3)

app.add_middleware(PrometheusMiddleware, app_name=PROMETHEUS_JOB_NAME, prefix=PROMETHEUS_JOB_NAME)
app.add_middleware(
    CORSMiddleware,
    allow_origins='*',
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
locale.setlocale(locale.LC_ALL, 'it_IT')

class CustomJsonFormatter(jsonlogger.JsonFormatter):
    def add_fields(self, log_record, record, message_dict):
        super(CustomJsonFormatter, self).add_fields(log_record, record, message_dict)
        message = log_record['message']
        message = message.replace("\"", "")
        message = message.split(" ")
        log_record['host'] = message[0]
        log_record['method'] = message[2]
        log_record['resource'] = message[3]
        log_record['http_version'] = message[4]
        log_record['status_code'] = message[5]
        log_record.pop('message', None)  

class Status(BaseModel):
    status: str

class HealthCheck(BaseModel):
    healthcheck: str

class RatingsDetailNumeric(BaseModel):
    period: str
    total_ratings: int
    avg: float
    total_ratings_difference: int
    avg_difference: float

def build_data(entrypoint_id, limit, interval, end_date, type):
    if(type == 'stacked'):
        if(interval == 'daily'):
            _end_date = end_date.strftime("%Y-%m-%d")
            ratings = HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).query_abs(entrypoint_id, _end_date, limit, interval, 'desc')
            retrieved_dates = [ratings['data']['ratings_' + interval][k]['day'] for k in range(len(ratings['data']['ratings_' + interval]))]
            return build_data_(ratings, retrieved_dates, limit, interval, end_date, 'day')
        elif(interval == 'weekly'):
            ratings = HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).query_abs(entrypoint_id, end_date, limit, interval, 'desc')
            retrieved_dates = [str(ratings['data']['ratings_' + interval][k]['year_week']) for k in range(len(ratings['data']['ratings_' + interval]))]
            return build_data_(ratings, retrieved_dates, limit, interval, end_date, 'year_week')
        elif(interval == 'monthly'):
            ratings = HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).query_abs(entrypoint_id, end_date, limit, interval, 'desc')
            retrieved_dates = [ratings['data']['ratings_' + interval][k]['year_month'] for k in range(len(ratings['data']['ratings_' + interval]))]
            return build_data_(ratings, retrieved_dates, limit, interval, end_date, 'year_month')
        elif(interval == 'yearly'):
            _end_date = end_date.strftime("%Y")
            ratings = HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).query_abs(entrypoint_id, _end_date, limit, interval, 'desc')
            retrieved_dates = [ratings['data']['ratings_' + interval][k]['year'] for k in range(len(ratings['data']['ratings_' + interval]))]
            return build_data_(ratings, retrieved_dates, limit, interval, end_date, 'year')
    elif(type == 'pie'):
        if(interval == 'daily'):
            end_date = datetime.strptime(end_date.split("T")[0], "%Y-%m-%d").strftime("%Y-%m-%d")
            ratings = HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).query_latest(entrypoint_id, limit, end_date, interval)
            return build_data_latest(ratings, interval)
        elif(interval == 'weekly'):
            week = datetime.strptime(end_date.split("T")[0], "%Y-%m-%d").strftime("%V")
            year = datetime.strptime(end_date.split("T")[0], "%Y-%m-%d").strftime("%G")
            end_date = str(int(year)*100 + int(week))
            ratings = HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).query_latest(entrypoint_id, limit, end_date, interval)
            return build_data_latest(ratings, interval)
        elif(interval == 'monthly'):
            month = datetime.strptime(end_date.split("T")[0], '%Y-%m-%d').strftime("%m")
            year = (datetime.strptime(end_date.split("T")[0], "%Y-%m-%d")).strftime("%Y")
            end_date = str(int(year)*100 + int(month))
            ratings = HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).query_latest(entrypoint_id, limit, end_date, interval)
            return build_data_latest(ratings, interval)
        elif(interval == 'yearly'):
            year = (datetime.strptime(end_date.split("T")[0], "%Y-%m-%d")).strftime("%Y")
            ratings = HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).query_latest(entrypoint_id, limit, year, interval)
            return build_data_latest(ratings, interval)
    elif(type == 'numeric'):
        if(interval == 'daily'):
            _end_date = datetime.strptime(end_date.split("T")[0], "%Y-%m-%d").strftime("%Y-%m-%d")
            ratings = HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).query_numeric(entrypoint_id, limit, _end_date, interval)
            retrieved_dates = [ratings['data']['ratings_' + interval][k]['day'] for k in range(len(ratings['data']['ratings_' + interval]))]
            return build_data_latest_numeric_(ratings, limit, datetime.strptime(end_date.split("T")[0], "%Y-%m-%d"), retrieved_dates, interval, 'day')
        elif(interval == 'weekly'):
            week = datetime.strptime(end_date.split("T")[0], "%Y-%m-%d").strftime("%V")
            year = datetime.strptime(end_date.split("T")[0], "%Y-%m-%d").strftime("%G")
            _end_date = str(int(year)*100 + int(week))
            ratings = HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).query_numeric(entrypoint_id, limit, _end_date, interval)
            retrieved_dates = [str(ratings['data']['ratings_' + interval][k]['year_week']) for k in range(len(ratings['data']['ratings_' + interval]))]
            return build_data_latest_numeric_(ratings, limit, datetime.strptime(end_date.split("T")[0], "%Y-%m-%d"), retrieved_dates, interval, 'year_week')
        elif(interval == 'monthly'):
            month = datetime.strptime(end_date.split("T")[0], '%Y-%m-%d').strftime("%m")
            year = (datetime.strptime(end_date.split("T")[0], "%Y-%m-%d")).strftime("%Y")
            _end_date = str(int(year)*100 + int(month))
            ratings = HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).query_numeric(entrypoint_id, limit, _end_date, interval)
            retrieved_dates = [ratings['data']['ratings_' + interval][k]['year_month'] for k in range(len(ratings['data']['ratings_' + interval]))]
            return build_data_latest_numeric_(ratings, limit, datetime.strptime(end_date.split("T")[0], "%Y-%m-%d"), retrieved_dates, interval, 'year_month')
        elif(interval == 'yearly'):
            _end_date = int(datetime.strptime(end_date.split("T")[0], '%Y-%m-%d').strftime("%Y"))
            ratings = HasuraClient(HASURA_ENDPOINT, HASURA_SECRET).query_numeric(entrypoint_id, limit, _end_date, interval)
            retrieved_dates = [ratings['data']['ratings_' + interval][k]['year'] for k in range(len(ratings['data']['ratings_' + interval]))]
            return build_data_latest_numeric_(ratings, limit, datetime.strptime(end_date.split("T")[0], "%Y-%m-%d"), retrieved_dates, interval, 'year')

@app.on_event("startup")
def startup_event():
    logging.info('Initializing API ...')
    logger = logging.getLogger('uvicorn.access')
    logHandler = logging.StreamHandler()
    formatter = CustomJsonFormatter('%(asctime)s %(message)s')
    logHandler.setFormatter(formatter)
    logger.addHandler(logHandler)

@app.on_event("shutdown")
def shutdown_event():
    print("Shutting down...")

@app.get('/status', response_model=Status, status_code=status.HTTP_200_OK)
def check_service_status():
    return {'status': 'Everything OK!'}

@app.get('/healthcheck', response_model=HealthCheck, status_code=status.HTTP_200_OK)
def perform_healthcheck():
    return {'healthcheck': 'Everything OK!'}

@app.get('/stats/{entrypoint_id}/ratings', responses=ratings_responses)
def get_chart(entrypoint_id: str, params: RatingsParams = Depends()):
    if not params.end_date:
        if params.interval == 'daily':
            params.end_date = datetime.today() - timedelta(days=1)
            limit = 14
        elif params.interval == 'weekly':
            params.end_date = datetime.today() - timedelta(weeks=1)
            limit = 12
        elif params.interval == 'monthly':
            params.end_date = datetime.today() - dateutil.relativedelta.relativedelta(months=1)
            limit = 12        
        elif(params.interval == 'yearly'):
            params.end_date = datetime.today() - dateutil.relativedelta.relativedelta(years=1)
            limit = 1
    elif not params.start_date:
        params.end_date = datetime.strptime(params.end_date.split("T")[0], "%Y-%m-%d")
        if params.interval == 'daily':
            limit = 14
        elif params.interval == 'weekly':
            limit = 12
        elif params.interval == 'monthly':
            limit = 12        
        elif(params.interval == 'yearly'):
            limit = 1
    else:
        params.start_date = datetime.strptime(params.start_date.split("T")[0], "%Y-%m-%d")
        params.end_date = datetime.strptime(params.end_date.split("T")[0], "%Y-%m-%d")
        if params.interval == 'daily':
            limit = (params.end_date - params.start_date).days
            limit = MAX_DAYS if limit+1 > int(MAX_DAYS) else abs(limit+1)
        elif params.interval == 'weekly':
            limit = ((params.end_date - timedelta(days=params.end_date.weekday())) - (params.start_date - timedelta(days=params.start_date.weekday()))) / 7
            limit = MAX_WEEKS if limit.days+1 > int(MAX_WEEKS) else abs(limit.days+1)
        elif params.interval == 'monthly':
            limit = (params.end_date.year - params.start_date.year) * 12 + (params.end_date.month - params.start_date.month)
            limit = MAX_MONTHS if limit+1 > int(MAX_MONTHS) else abs(limit+1)
        elif params.interval == 'yearly':
            limit = int(params.end_date.strftime("%Y")) - int(params.start_date.strftime("%Y"))
            limit = MAX_YEARS if limit+1 > int(MAX_YEARS) else abs(limit+1)

    if params.chart_format == 'cyclotron':
        return build_data(entrypoint_id, limit, params.interval, params.end_date, 'stacked')
    elif params.chart_format == 'highcharts':
        data = build_data(entrypoint_id, limit, params.interval, params.end_date, 'stacked')
        return build_data_highcharts_bar(data, params.chart_type, params.interval)

@app.get('/stats/{entrypoint_id}/ratings/detail', responses=rating_detail_responses)
def get_chart_of_specific_period(entrypoint_id: str, params: RatingDetailParams = Depends()):
    if not params.date:
        if params.interval == 'daily':
            params.date = (datetime.today() - timedelta(days=1)).strftime("%Y-%m-%d")
        elif params.interval == 'weekly':
            params.date = (datetime.today() - timedelta(weeks=1)).strftime("%Y-%m-%d")
        elif params.interval == 'monthly':
            params.date = (datetime.today() - dateutil.relativedelta.relativedelta(months=1)).strftime("%Y-%m-%d")
        elif params.interval == 'yearly':
            params.date = (datetime.today() - dateutil.relativedelta.relativedelta(years=1)).strftime("%Y-%m-%d")
            
    if params.chart_format == 'cyclotron':
        return build_data(entrypoint_id, 1, params.interval, params.date, 'pie')
    elif params.chart_format == 'highcharts':
        series = build_data(entrypoint_id, 1, params.interval, params.date, 'pie')
        return build_data_highcharts_pie(series, params.interval, params.date)
        
@app.get('/stats/{entrypoint_id}/ratings/detail/numeric', response_model=RatingsDetailNumeric)
def get_single_stat(entrypoint_id: str, params: SingleStatParams = Depends()):
    if not params.date:
        if params.interval == 'daily':
            params.date = (datetime.today() - timedelta(days=1)).strftime("%Y-%m-%d")
        elif params.interval == 'weekly':
            params.date = (datetime.today() - timedelta(weeks=1)).strftime("%Y-%m-%d")
        elif params.interval == 'monthly':
            params.date = (datetime.today() - dateutil.relativedelta.relativedelta(months=1)).strftime("%Y-%m-%d")
        elif params.interval == 'yearly':
            params.date = (datetime.today() - dateutil.relativedelta.relativedelta(years=1)).strftime("%Y-%m-%d")
    return build_data(entrypoint_id, 2, params.interval, params.date, 'numeric')

app.add_route("/metrics", handle_metrics)

if __name__ == "__main__":
    uvicorn.run(app, host=FASTAPI_APP_HOST, port=int(FASTAPI_APP_PORT), proxy_headers=True, forwarded_allow_ips='*', access_log=False)