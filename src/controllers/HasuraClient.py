import requests
import json
from datetime import datetime
from isoweek import Week

class HasuraClient:
    def __init__(self, endpoint, secret):
        self.endpoint = endpoint
        self.secret = secret
    
    def query_abs(self, entrypoint, end_date, limit, period, order):
        if(period == 'daily'):
            filter_name = 'day'
            interval = 'day'
        elif(period == 'weekly'):
            filter_name = 'year_week'
            interval = 'year_week'
            week = int(datetime.strptime(end_date.isoformat().split("T")[0], "%Y-%m-%d").strftime("%V"))
            year = int(datetime.strptime(end_date.isoformat().split("T")[0], "%Y-%m-%d").strftime("%G"))
            end_date = str(year*100 + week)
        elif(period == 'monthly'):
            filter_name = 'year_month'
            interval = 'year_month'
            month = int(datetime.strptime(end_date.isoformat().split("T")[0], '%Y-%m-%d').strftime("%m"))
            year = int((datetime.strptime(end_date.isoformat().split("T")[0], "%Y-%m-%d")).strftime("%Y"))
            end_date = str(year*100 + month)
        elif(period == 'yearly'):
            filter_name = 'year'
            interval = 'year'
        
        query = """
            query getRatingsByEntrypoint {
                ratings_""" + period + """ (where: {entrypoint_id: {_eq: \"""" + entrypoint + """\"}, """ + filter_name + """: {_lte: \"""" + end_date + """\"}}, limit:""" + str(limit) + """, order_by: {""" + filter_name + """:""" + order + """}) {
                     """ + interval + """
                    values_1_count
                    values_2_count
                    values_3_count
                    values_4_count
                    values_5_count
                    values_avg
                }
            }    
        """
        print(query)
        return self.get(query)

    def query_latest(self, entrypoint, limit, end_date, period):
        if(period == 'daily'):
            filter_name = 'day'
            interval = 'day'
        elif(period == 'weekly'):
            filter_name = 'year_week'
            interval = 'year_week'
        elif(period == 'monthly'):
            filter_name = 'year_month'
            interval = 'year_month'
        elif(period == 'yearly'):
            filter_name = 'year'
            interval = 'year'
            
        query = """
            query getRatingsByEntrypoint {
                ratings_""" + period + """ (where: {entrypoint_id: {_eq: \"""" + entrypoint + """\"}, """ + filter_name + """: {_eq: \"""" + end_date + """\"}}) {
                    """ + interval + """
                    values_1_count
                    values_2_count
                    values_3_count
                    values_4_count
                    values_5_count
                    values_avg
                }
            }
        """
        return self.get(query)

    def query_numeric(self, entrypoint, limit, end_date, period):
        if(period == 'daily'):
            filter_name = 'day'
            interval = 'day'
            order = '{day: desc}'
        elif(period == 'weekly'):
            filter_name = 'year_week'
            interval = 'year_week'
            order = '{year_week: desc}'
        elif(period == 'monthly'):
            filter_name = 'year_month'
            interval = 'year_month'
            order = '{year_month: desc}'
        elif(period == 'yearly'):
            filter_name = 'year'
            interval = 'year'
            order = '{year: desc}'
            
        query = """
            query getRatingsByEntrypoint {
                ratings_""" + period + """ (where: {entrypoint_id: {_eq: \"""" + entrypoint + """\"},""" + filter_name + """: {_lte: \"""" + str(end_date) + """\"}}, limit:""" + str(limit) + """, order_by: """ + order + """) {
                    """ + interval + """
                    values_1_count
                    values_2_count
                    values_3_count
                    values_4_count
                    values_5_count
                    values_avg
                }
            }
        """
        return self.get(query)

    def get(self, query):
        headers = {
            'Content-Type': 'application/json',
            'X-Hasura-Admin-Secret': self.secret
        }
        response = requests.post(self.endpoint, json={'query': query}, headers=headers)
        if response.status_code == 200:
            print(response.json())
            return response.json()
        else:
            raise Exception("Query failed to run by returning code of {}. {}".format(response.status_code, query))