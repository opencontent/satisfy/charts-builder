ratings_responses = {
    200: {
        "description": "Success",
        "content": {
            "application/json": {
                "examples": {
                    "cyclotron": {
                        "summary": "Cyclotron Format",
                        "value": [
                            {
                                "period": "2022-02-16",
                                "1 stella": 10,
                                "2 stelle": 23,
                                "3 stelle": 17,
                                "4 stelle": 10,
                                "5 stelle": 26,
                                "media": 3.2209,
                                "deviazione standard": 1.4175
                            }
                        ]
                    },
                    "highcharts_abs": {
                        "summary": "Highcharts Format Absolute",
                        "value": [
                            {
                                "type": "highcharts",
                                "config": {
                                    "chart": {
                                        "type": "column"
                                    },
                                    "title": {
                                        "text": "Valutazioni giornaliere"
                                    },
                                    "xAxis": {
                                        "categories": ["2022-02-16","2022-02-17","2022-02-18","2022-02-19","2022-02-20","2022-02-21","2022-02-22","2022-02-23","2022-02-24","2022-02-25","2022-02-26","2022-02-27","2022-02-28","2022-03-01"]
                                    },
                                    "yAxis": [
                                        {
                                        "min": 0,
                                        "title": {
                                            "text": "Numero valutazioni totali"
                                        },
                                        "stackLabels": {
                                            "enabled": True,
                                            "style": {
                                            "fontWeight": "bold",
                                            "color": "(Highcharts.defaultOptions.title.style && Highcharts.defaultOptions.title.style.color) || 'gray'"
                                            }
                                        }
                                        },
                                        {
                                        "labels": {
                                            "format": "{value}",
                                            "style": {
                                            "color": "Highcharts.getOptions().colors[2]"
                                            }
                                        },
                                        "title": {
                                            "text": "Media",
                                            "style": {
                                            "color": "Highcharts.getOptions().colors[2]"
                                            }
                                        },
                                        "opposite": True
                                        },
                                        {
                                        "labels": {
                                            "format": "{value}",
                                            "style": {
                                            "color": "Highcharts.getOptions().colors[2]"
                                            }
                                        },
                                        "title": {
                                            "text": "Deviazione Standard",
                                            "style": {
                                            "color": "Highcharts.getOptions().colors[2]"
                                            }
                                        },
                                        "opposite": True
                                        }
                                    ],
                                    "tooltip": {
                                        "headerFormat": "<b>{point.x}</b><br/>",
                                        "pointFormat": "{series.name}: {point.y}<br/>Total: {point.stackTotal}"
                                    },
                                    "plotOptions": {
                                        "column": {
                                        "stacking": "normal",
                                        "dataLabels": {
                                            "enabled": True
                                        }
                                        }
                                    },
                                    "series": [
                                        {
                                            "name": "5 stelle",
                                            "color": "#03045e",
                                            "data": [26,17,19,22,11,14,25,10,12,12,16,12,21,12]
                                        },
                                        {
                                            "name": "4 stelle",
                                            "color": "#0077b6",
                                            "data": [10,12,31,16,14,20,23,15,17,22,13,20,13,16]
                                        },
                                        {
                                            "name": "3 stelle",
                                            "color": "#00b4d8",
                                            "data": [17,13,15,19,19,16,19,11,20,18,19,14,15,15]
                                        },
                                        {
                                            "name": "2 stelle",
                                            "color": "#90e0ef",
                                            "data": [23,15,19,12,21,24,13,20,18,16,12,17,16,17]
                                        },
                                        {
                                            "name": "1 stella",
                                            "color": "#caf0f8",
                                            "data": [10,16,15,14,21,15,17,19,13,24,16,20,17,21]
                                        },
                                        {
                                            "type": "spline",
                                            "shadow": True,
                                            "name": "media",
                                            "yAxis": 1,
                                            "data": [3.2209,2.9863,3.202,3.241,2.686,2.9326,3.268,2.6933,2.9625,2.8043,3.0132,2.8434,3.061,2.7654]
                                        },
                                        {
                                            "type": "spline",
                                            "shadow": True,
                                            "color": "#A9A9A9",
                                            "name": "deviazione standard",
                                            "yAxis": 2,
                                            "data": [1.4175,1.4758,1.3557,1.4192,1.3404,1.3391,1.4252,1.3854,1.2985,1.3928,1.4188,1.4012,1.4845,1.4078]
                                        }
                                    ]
                                }
                            }
                        ]
                    },
                    "highcharts_perc": {
                        "summary": "Highcharts Format Percentage",
                        "value": [
                            {
                                "type": "highcharts",
                                "config": {
                                    "chart": {
                                        "type": "column"
                                    },
                                    "title": {
                                        "text": "Valutazioni giornaliere percentuali"
                                    },
                                    "xAxis": {
                                        "categories": ["2022-02-16","2022-02-17","2022-02-18","2022-02-19","2022-02-20","2022-02-21","2022-02-22","2022-02-23","2022-02-24","2022-02-25","2022-02-26","2022-02-27","2022-02-28","2022-03-01"]
                                    },
                                    "yAxis": {
                                        "min": 0,
                                        "title": {
                                        "text": "Numero valutazioni totali percentuali"
                                        }
                                    },
                                    "tooltip": {
                                        "pointFormat": "<span style=\"color:{series.color}\">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>",
                                        "shared": True
                                    },
                                    "plotOptions": {
                                        "column": {
                                        "stacking": "percent",
                                        "dataLabels": {
                                            "enabled": True
                                        }
                                        }
                                    },
                                    "series": [
                                        {
                                            "name": "5 stelle",
                                            "color": "#03045e",
                                            "data": [26,17,19,22,11,14,25,10,12,12,16,12,21,12]
                                        },
                                        {
                                            "name": "4 stelle",
                                            "color": "#0077b6",
                                            "data": [10,12,31,16,14,20,23,15,17,22,13,20,13,16]
                                        },
                                        {
                                            "name": "3 stelle",
                                            "color": "#00b4d8",
                                            "data": [17,13,15,19,19,16,19,11,20,18,19,14,15,15]
                                        },
                                        {
                                            "name": "2 stelle",
                                            "color": "#90e0ef",
                                            "data": [23,15,19,12,21,24,13,20,18,16,12,17,16,17]
                                        },
                                        {
                                            "name": "1 stella",
                                            "color": "#caf0f8",
                                            "data": [10,16,15,14,21,15,17,19,13,24,16,20,17,21]
                                        }
                                    ]
                                }
                            }
                        ]
                    }
                }
            }
        }
    },
}

rating_detail_responses = {
    200: {
        "description": "Success",
        "content": {
            "application/json": {
                "examples": {
                    "cyclotron": {
                        "summary": "Cyclotron Format",
                        "value": [
                            {
                                "star": "5 stelle",
                                "number": 12
                            },
                            {
                                "star": "4 stelle",
                                "number": 16
                            },
                            {
                                "star": "3 stelle",
                                "number": 15
                            },
                            {
                                "star": "2 stelle",
                                "number": 17
                            },
                            {
                                "star": "1 stella",
                                "number": 21
                            }
                        ]
                    },
                    "highcharts": {
                        "summary": "Highcharts Format",
                        "value": [
                            {
                                "type": "highcharts",
                                "config": {
                                    "chart": {
                                        "plotBackgroundColor": None,
                                        "plotBorderWidth": None,
                                        "plotShadow": False,
                                        "type": "pie"
                                    },
                                    "title": {
                                        "text": "Valutazioni del: 2022-03-01"
                                    },
                                    "tooltip": {
                                        "pointFormat": "{series.name}: <b>{point.y}</b>"
                                    },
                                    "accessibility": {
                                        "point": {
                                        "valueSuffix": "%"
                                        }
                                    },
                                    "plotOptions": {
                                        "pie": {
                                        "allowPointSelect": True,
                                        "cursor": "pointer",
                                        "dataLabels": {
                                            "enabled": True,
                                            "format": "<b>{point.name}</b>: {point.percentage:.1f} %"
                                        },
                                        "showInLegend": True
                                        }
                                    },
                                    "series": [
                                        {
                                            "name": "Numero stelle",
                                            "colorByPoint": True,
                                            "data": [
                                                {
                                                    "name": "1 stella",
                                                    "y": 21,
                                                    "color": "#caf0f8"
                                                },
                                                {
                                                    "name": "2 stelle",
                                                    "y": 17,
                                                    "color": "#90e0ef"
                                                },
                                                {
                                                    "name": "3 stelle",
                                                    "y": 15,
                                                    "color": "#00b4d8"
                                                },
                                                {
                                                    "name": "4 stelle",
                                                    "y": 16,
                                                    "color": "#0077b6"
                                                },
                                                {
                                                    "name": "5 stelle",
                                                    "y": 12,
                                                    "color": "#03045e"
                                                }
                                            ]
                                        }
                                    ]
                                }
                            }
                        ]
                    },
                }
            }
        }
    },
}
