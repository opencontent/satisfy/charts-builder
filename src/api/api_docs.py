from datetime import datetime, timedelta
from tracemalloc import start
from fastapi import Query

class RatingsParams:
    def __init__(
        self,
        interval: str = Query(default='daily', description="Livello di aggregazione dei ratings (daily, weekly, monthly o yearly)"),
        start_date: str = Query(default=None, description="Data d'inizio (standard ISO8601) a partire dalla quale visualizzare le statistiche"),
        end_date: str = Query(default=None, description="Data di fine (standard ISO8601) entro la quale visualizzare le statistiche"),
        chart_format: str = Query(default='cyclotron', description="Formato del dato ritornato, se \"cyclotron\" viene ritornata solo la serie di dati, se \"highcharts\"  allora viene ritornata anche la configurazione del grafico"),
        chart_type: str = Query(default='abs', description='Se il formato selezionato è \"highcharts\" indicare il tipo di grafico (abs per i valori assoluti, perc per i valori in percentuale)')
    ):
        self.interval = interval
        self.start_date = start_date
        self.end_date = end_date
        self.chart_format = chart_format
        self.chart_type = chart_type 

class RatingDetailParams:
    def __init__(
        self,
        interval: str = Query(default='daily', description="Livello di aggregazione del rating (daily, weekly, monthly o yearly)"),
        date: str = Query(default=None, description="In base all'interval settato, verrà ricavato da date (standard ISO8601) il giorno, la settimana, il mese o l'anno di cui visualizzare il rating"),
        chart_format: str = Query(default='cyclotron', description="Formato del dato ritornato, se \"cyclotron\" viene ritornata solo la serie di dati, se \"highcharts\" allora viene ritornata anche la configurazione del grafico")
    ):
        self.interval = interval
        self.date = date 
        self.chart_format = chart_format

class SingleStatParams:
    def __init__(
        self,
        interval: str = Query(default='daily', description="Livello di aggregazione del rating (daily, weekly, monthly o yearly)"),
        date: str = Query(default=None, description="In base all'interval settato, verrà ricavato da date (standard ISO8601) il giorno, la settimana, il mese o l'anno di cui visualizzare il rating")
    ):
        self.interval = interval
        self.date = date