from datetime import datetime, timedelta
from isoweek import Week

import calendar
import numpy as np
import dateutil.relativedelta

def get_end_date_from_calendar_week(year, calendar_week):       
    monday = datetime.strptime(f'{year}-{calendar_week}-1', "%G-%V-%u").date()
    return monday + timedelta(days=6.9)

def build_data_latest_numeric_(ratings, limit, end_date, retrieved_dates, interval, period):
    data = {}
    if(len(ratings['data']['ratings_' + interval]) != 0):
        if(len(ratings['data']['ratings_' + interval]) == 2):
            for i in range(limit):
                if interval == 'daily':
                    current_date = (datetime.strptime(end_date.isoformat().split("T")[0], "%Y-%m-%d") - timedelta(days=i)).strftime("%Y-%m-%d")
                elif interval == 'weekly':
                    current_date = datetime.strptime(end_date.isoformat().split("T")[0], "%Y-%m-%d") - timedelta(weeks=i)
                    current_date = current_date.strftime("%G") + current_date.strftime("%V")            
                elif interval == 'monthly':
                    current_date = (datetime.strptime(end_date.isoformat().split("T")[0], "%Y-%m-%d") - dateutil.relativedelta.relativedelta(months=i))
                    current_date = current_date.strftime("%Y") + current_date.strftime("%m")
                    current_date = int(current_date)
                elif interval == 'yearly':
                    current_date = (datetime.strptime(end_date.isoformat().split("T")[0], "%Y-%m-%d") - dateutil.relativedelta.relativedelta(years=i)).strftime("%Y")
                    current_date = int(current_date)
                if(i==0):
                    if interval == 'daily':
                        data['period'] = datetime.strptime(end_date.isoformat().split("T")[0], "%Y-%m-%d").strftime("%Y-%m-%d")
                    elif interval == 'weekly':
                        data['period'] = get_end_date_from_calendar_week(int(str(current_date)[0:4]), int(str(current_date)[4:6])).strftime("%Y-%m-%d")
                    elif interval == 'monthly':
                        data['period'] = calendar.month_name[int(str(current_date)[4:6])] + " " + str(current_date)[0:4]
                    elif interval == 'yearly':
                        data['period'] = current_date
                    if(current_date in retrieved_dates):
                        rating_target = [rating for rating in ratings['data']['ratings_' + interval] if rating[period] == (current_date if interval == 'daily' else int(current_date))]
                        total_ratings = 0
                        for j in range(1,6):
                            total_ratings += rating_target[0]['values_' + str(j) + '_count']
                        data['total_ratings'] = total_ratings
                        data['avg'] = round(rating_target[0]['values_avg'], 4)
                    else:
                        data['total_ratings'] = 0
                        data['avg'] = 0 
                else:
                    if(current_date in retrieved_dates):
                        rating_target = [rating for rating in ratings['data']['ratings_' + interval] if rating[period] == (current_date if interval == 'daily' else int(current_date))]
                        total_ratings = 0
                        for j in range(1,6):
                            total_ratings += rating_target[0]['values_' + str(j) + '_count']
                        data['total_ratings_difference'] = data['total_ratings'] - total_ratings
                        data['avg_difference'] = round(data['avg'] - rating_target[0]['values_avg'], 4)
                    else:
                        data['total_ratings_difference'] = data['total_ratings']
                        data['avg_difference'] = round(data['avg'], 4)
        else:
            if interval == 'daily':
                current_date = datetime.strptime(end_date.isoformat().split("T")[0], "%Y-%m-%d").strftime("%Y-%m-%d")
                data['period'] = current_date
            elif interval == 'weekly':
                current_date = datetime.strptime(end_date.isoformat().split("T")[0], "%Y-%m-%d") - timedelta(weeks=i)
                current_date = current_date.strftime("%G") + current_date.strftime("%V")      
                data['period'] = get_end_date_from_calendar_week(int(current_date[0:4]), int(current_date[4:6])).strftime("%Y-%m-%d")
            elif interval == 'monthly':
                current_date = datetime.strptime(end_date.isoformat().split("T")[0], "%Y-%m-%d")
                current_date = current_date.strftime("%Y") + current_date.strftime("%m")
                data['period'] = calendar.month_name[int(current_date[4:6])] + " " + current_date[0:4]
                current_date = int(current_date)
            elif interval == 'yearly':
                current_date = datetime.strptime(end_date.isoformat().split("T")[0], "%Y-%m-%d").strftime("%Y")
                data['period'] = current_date
                current_date = int(current_date)
            if(current_date in retrieved_dates):
                rating_target = [rating for rating in ratings['data']['ratings_' + interval] if rating[period] == (current_date if interval == 'daily' else int(current_date))]
                total_ratings = 0
                for j in range(1,6):
                    total_ratings += rating_target[0]['values_' + str(j) + '_count']
                data['total_ratings'] = total_ratings
                data['avg'] = round(rating_target[0]['values_avg'], 4)
                data['total_ratings_difference'] = data['total_ratings']
                data['avg_difference'] = data['avg']
            else:
                data['total_ratings'] = 0
                data['avg'] = 0
                total_ratings = 0
                for j in range(1,6):
                    total_ratings += ratings['data']['ratings_' + interval][0]['values_' + str(j) + '_count']
                data['total_ratings_difference'] = -total_ratings
                data['avg_difference'] = -round(ratings['data']['ratings_' + interval][0]['values_avg'], 4)
    else:
        if interval == 'daily':
            data['period'] = datetime.strptime(end_date.isoformat().split("T")[0], "%Y-%m-%d").strftime("%Y-%m-%d")
        elif interval == 'weekly':
            current_date = datetime.strptime(end_date.isoformat().split("T")[0], "%Y-%m-%d")
            current_date_fmt = current_date.strftime("%G") + "-" + current_date.strftime("%V")      
            data['period'] = get_end_date_from_calendar_week(int(current_date_fmt.split("-")[0]), int(current_date_fmt.split("-")[1])).strftime("%Y-%m-%d")
        elif interval == 'monthly':
            current_date = datetime.strptime(end_date.isoformat().split("T")[0], "%Y-%m-%d")
            data['period'] = calendar.month_name[int(current_date.strftime("%m"))] + " " + current_date.strftime("%Y")
        elif interval == 'yearly':
             data['period'] = datetime.strptime(end_date.isoformat().split("T")[0], "%Y-%m-%d").strftime("%Y") 
        data['total_ratings'] = 0
        data['avg'] = 0
        data['total_ratings_difference'] = 0
        data['avg_difference'] = 0              
    return data

def build_data_latest(ratings, interval):
    series = list()
    if(len(ratings['data']['ratings_' + interval]) != 0):
        for i in range(len(ratings['data']['ratings_' + interval])):
            data = {}
            for j in range(1,6):
                data['star'] = str(j) + (' stella' if j==1 else ' stelle')
                data['number'] = ratings['data']['ratings_' + interval][i]['values_' + str(j) + '_count']
                series.append(data)   
                data = {} 
        series.reverse()
    else:
        data = {}
        for j in range(1,6):
            data['star'] = str(j) + (' stella' if j==1 else ' stelle')
            data['number'] = 0
            series.append(data)
            data = {}
        series.reverse()
    return series

def build_data_(ratings, retrieved_dates, limit, interval, end_date, period):
    series = list()
    for i in range(limit):
        if interval == 'daily':
            current_date = (datetime.strptime(end_date.isoformat().split("T")[0], "%Y-%m-%d") - timedelta(days=i)).strftime("%Y-%m-%d")
            series.append({'period': current_date})
        elif interval == 'weekly':
            current_date = datetime.strptime(end_date.isoformat().split("T")[0], "%Y-%m-%d") - timedelta(weeks=i)
            current_date = current_date.strftime("%G") + current_date.strftime("%V")
            series.append({'period': get_end_date_from_calendar_week(int(current_date[0:4]), int(current_date[4:6])).strftime("%Y-%m-%d")})
        elif interval == 'monthly':
            current_date = (datetime.strptime(end_date.isoformat().split("T")[0], "%Y-%m-%d") - dateutil.relativedelta.relativedelta(months=i))
            current_date = current_date.strftime("%Y") + current_date.strftime("%m")
            series.append({'period': calendar.month_name[int(current_date[4:6])] + " " + current_date[0:4]})
            current_date = int(current_date)
        elif interval == 'yearly':
            current_date = (datetime.strptime(end_date.isoformat().split("T")[0], "%Y-%m-%d") - dateutil.relativedelta.relativedelta(years=i)).strftime("%Y")
            series.append({'period': current_date})
            current_date = int(current_date)
        if(current_date in retrieved_dates):
            rating_target = [rating for rating in ratings['data']['ratings_' + interval] if rating[period] == (current_date if interval == 'daily' else int(current_date))]
            for j in range(1,6):
                series[i][str(j) + (' stella' if j==1 else ' stelle')] = rating_target[0]['values_' + str(j) + '_count']
            series[i]['media'] = round(rating_target[0]['values_avg'], 4)
            if series[i]['media'] != 0:
                tmp = [[j]*series[i][str(j) + (' stella' if j==1 else ' stelle')] for j in range(1,6)]
                tmp_2 = round(np.std(np.array([x for l in tmp for x in l])),4)
                series[i]['deviazione standard'] = [series[i]['media'] - tmp_2, series[i]['media'] + tmp_2]
                # series[i]['deviazione standard'] = round(np.std(np.array([x for l in tmp for x in l])),4)         
            else:
                series[i]['deviazione standard'] = 0
        else:
            for j in range(1,6):
                series[i][str(j) + (' stella' if j==1 else ' stelle')] = 0
            series[i]['media'] = 0
            series[i]['deviazione standard'] = 0
    series.reverse()
    return series

def build_data_highcharts_bar(data, chart_type, interval):
    x_axis = [dict_el['period'] for dict_el in data]
    series = []

    colors = ['#caf0f8', '#90e0ef', '#00b4d8', '#0077b6', '#03045e']
    # colors = ['#fb7979', '#fbb46f', '#fbd66b', '#bed27e', '#66b57c']
    for i in range(1,6):
        series.append({
            'name': str(i) + ' stella' if i==1 else str(i) + ' stelle',
            'color': colors[i-1],
            'data': [dict_el[(str(i) + ' stella') if i==1 else (str(i) + ' stelle')] for dict_el in data]
        })
    series.reverse()

    if(chart_type == 'abs'):
        series.append({
            'type': 'spline',
            'shadow': True, 
            'name': 'media', 
            'yAxis': 1,
            'zIndex': 1,
            'data': [dict_el['media'] for dict_el in data]
        })

        series.append({
            'type': 'errorbar',
            'shadow': True, 
            'color': '#A9A9A9',
            'lineWidth': 3,
            'name': 'deviazione standard', 
            'yAxis': 1,
            'data': [dict_el['deviazione standard'] for dict_el in data]
        })
        
        freq = 'giornaliere'
        if interval == 'daily':
            freq = 'giornaliere'
        elif interval == 'weekly':
            freq = 'settimanali'
        elif interval == 'monthly':
            freq = 'mensili'
        elif interval == 'yearly':
            freq = 'annuali'  

        chart_schema = {
            'chart': {
                'type': 'column'
            },
            'title': {
                'text': 'Valutazioni ' + freq
            },
            'xAxis': {
                'categories': x_axis
            },
            'yAxis': [{
                'min': 0,
                'title': {
                    'text': 'Numero valutazioni totali'
                },
                'stackLabels': {
                    'enabled': True,
                    'style': {
                        'fontWeight': 'bold',
                        'color': "(Highcharts.defaultOptions.title.style && Highcharts.defaultOptions.title.style.color) || 'gray'"
                    }
                }
            }, {
                # 'tickPositions': list(range(1,6)),
                'labels': {
                    'format': '{value}',
                    'style': {
                        'color': 'Highcharts.getOptions().colors[2]'
                    }
                },
                # 'min': 0,
                # 'max': 10,
                # 'startOnTick': False,
                # 'endOnTick': False,                  
                'title': {
                    'text': 'Media e Deviazione Standard',
                    'style': {
                        'color': 'Highcharts.getOptions().colors[2]'
                    }
                },
                'opposite': True
            }
            # , {
            #     # 'tickPositions': list(range(1,6)),
            #     'labels': {
            #         'format': '{value}',
            #         'style': {
            #             'color': 'Highcharts.getOptions().colors[2]'
            #         }
            #     },
            #     'title': {
            #         'text': 'Deviazione Standard',
            #         'style': {
            #             'color': 'Highcharts.getOptions().colors[2]'
            #         }
            #     },
            #     'opposite': True
            # }
            ],
            'tooltip': {
                'headerFormat': '<b>{point.x}</b><br/>',
                'pointFormat': '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
            },
            'plotOptions': {
                'column': {
                    'stacking': 'normal',
                    'dataLabels': {
                        'enabled': True
                    }
                }
            },
            'series': series
        }
    elif(chart_type == 'perc'):
        freq = 'giornaliere'
        if interval == 'daily':
            freq = 'giornaliere'
        elif interval == 'weekly':
            freq = 'settimanali'
        elif interval == 'monthly':
            freq = 'mensili'
        elif interval == 'yearly':
            freq = 'annuali'
        
        chart_schema = {
            'chart': {
                'type': 'column'
            },
            'title': {
                'text': 'Valutazioni ' +  freq  + ' percentuali'
            },
            'xAxis': {
                'categories': x_axis
            },
            'yAxis': {
                'min': 0,
                'title': {
                    'text': 'Numero valutazioni totali percentuali'
                }
            },
            'tooltip': {
                'pointFormat': '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
                'shared': True
            },
            'plotOptions': {
                'column': {
                    'stacking': 'percent',
                    'dataLabels': {
                        'enabled': True
                    }
                },
            },
            'series': series
        }

    return [{ 'type': 'highcharts', 'config': chart_schema }]

def build_data_highcharts_pie(series, interval, date):
    freq = 'giornata'
    date_structure = ''
    if interval == 'daily':
        freq = 'del'
        date_structure = datetime.strptime(date.split("T")[0], "%Y-%m-%d").strftime("%Y-%m-%d")
    elif interval == 'weekly':
        freq = 'settimana del'
        date_structure = Week(int(datetime.strptime(date.split("T")[0], "%Y-%m-%d").strftime("%Y")), int(datetime.strptime(date.split("T")[0], "%Y-%m-%d").strftime("%V"))).sunday().strftime("%Y-%m-%d")
    elif interval == 'monthly':
        freq = 'di'
        date_structure = datetime.strptime(date.split("T")[0], '%Y-%m-%d').strftime("%B") + " " + datetime.strptime(date.split("T")[0], '%Y-%m-%d').strftime("%Y")
    elif interval == 'yearly':
        freq = 'del'
        date_structure = datetime.strptime(date.split("T")[0], '%Y-%m-%d').strftime("%Y")

    colors = ['#caf0f8', '#90e0ef', '#00b4d8', '#0077b6', '#03045e']
    # colors = ['#fb7979', '#fbb46f', '#fbd66b', '#bed27e', '#66b57c']
    series.reverse()
    for idx, dict_el in enumerate(series):
        dict_el['name'] = dict_el.pop('star')
        dict_el['y'] = dict_el.pop('number')
        dict_el['color'] = colors[idx]

    chart_schema = {
        'chart': {
            'plotBackgroundColor': None,
            'plotBorderWidth': None,
            'plotShadow': False,
            'type': 'pie'
        },
        'title': {
            'text': 'Valutazioni ' + freq + ": " + date_structure 
        },
        'tooltip': {
            'pointFormat': '{series.name}: <b>{point.y}</b>'
        },
        'accessibility': {
            'point': {
                'valueSuffix': '%'
            }
        },
        'plotOptions': {
            'pie': {
                'allowPointSelect': True,
                'cursor': 'pointer',
                'dataLabels': {
                    'enabled': True,
                    'format': '<b>{point.name}</b>: {point.percentage:.1f} %'
                },
                'showInLegend': True
            }
        },
        'series': [{
            'name': 'Numero stelle',
            'colorByPoint': True, 
            'data': series
        }]
    }
    return [{ 'type': 'highcharts', 'config': chart_schema }]