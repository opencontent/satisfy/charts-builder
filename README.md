# Charts-builder

Questo componente è parte di Satisfy, una piattaforma per la rilevazione della Customer Satisfaction mediante un classico sistema a stellette.

## Cosa fa

Questo componente interroga il database di Hasura per ottenere le aggregazioni giornaliere, settimanali, mensili e annuali. Successivamente, i dati vengono formattati per Highcharts e infine restituiti per essere rappresentati sui grafici creati con quest'ultimo.

## Documentazione

Le API disponibili vengono esposte sul path `/docs`

## Monitoring

Il microservizio espone anche in formato prometheus le metriche statistiche generiche sulla durata delle richieste.

## Deploy

Il microservizio è disponibile come app docker pronta all'uso.

Sul path `/healthcheck` è disponibile una url di controllo per verificare che l'applicazione sta funzionando correttamente.

## Configurazione

Il microservizio è configurabile mediante variabili di ambiente

| Nome                | Default                               | Required | Description                                                                                          |
| ------------------- | ------------------------------------- | -------- | ---------------------------------------------------------------------------------------------------- |
| FASTAPI_APP_HOST    | 0.0.0.0                               | Yes      | Indirizzo al quale si espone il microservizio                                                        |
| FASTAPI_APP_PORT    | 5006                                  | Yes      | Porta alla quale si espone il microservizio                                                          |
| PROMETHEUS_JOB_NAME | satisfy_charts_builder                | Yes      | Identificativo delle metriche esposte dal microservizio                                              |
| HASURA_ENDPOINT     | https://satisfy.hasura.app/v1/graphql | Yes      | Endpoint del db interrogato per ottenere le aggregazioni giornaliere, settimanali, mensili e annuali |
| HASURA_SECRET       | **********************                | Yes      | Password del db dove vengono salvate le aggregazioni giornalier, settimanali, mensili e annuali      |
| MAX_DAYS            | 180                                   | Yes      | Numero massimo di aggregazioni giornaliere che il microservizio può restituire                       |
| MAX_WEEKS           | 52                                    | Yes      | Numero massimo di aggregazioni settimanali che il servizio può restituire                            |
| MAX_MONTHS          | 18                                    | Yes      | Numero massimo di aggregazioni mensili che il servizio può restituire                                |
| MAX_YEARS           | 3                                     | Yes      | Numero massimo di aggregazioni annuali che il servizio può restituire                                |