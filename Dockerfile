# For more information, please refer to https://aka.ms/vscode-docker-python
FROM python:3.8-slim-buster

ARG USERNAME=charts-builder
ARG USER_UID=1000
ARG USER_GID=$USER_UID

# Create the user
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    # [Optional] Add sudo support. Omit if you don't need to install software after connecting.
    && apt-get update \
    && apt-get install -y sudo \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME
# [Optional] Set the default user. Omit if you want to keep the default as root.
USER $USERNAME

RUN sudo apt-get update
RUN sudo apt-get install -y locales locales-all
ENV LC_ALL it_IT.UTF-8
ENV LANG it_IT.UTF-8
ENV LANGUAGE it_IT.UTF-8

EXPOSE 5006

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

# Install pip requirements
COPY requirements.txt .
RUN python -m pip install -r requirements.txt

WORKDIR /app

RUN mkdir src
RUN mkdir src/controllers
RUN mkdir src/api
RUN mkdir src/utils

COPY /src/charts-builder.py /app/src 
COPY /src/controllers/HasuraClient.py /app/src/controllers
COPY /src/api/api_responses.py /app/src/api
COPY /src/api/api_docs.py /app/src/api
COPY /src/utils/data_builder.py /app/src/utils

CMD ["python3", "src/charts-builder.py"]
